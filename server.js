const express = require('express')
const path = require('path')
const app = express()

let HTTPDOMAIN

if (process.env.NODE_ENV === 'production') {
    // redirects HTTP to HTTPS
    app.enable('trust proxy')
    app.use((req, res, next) => {
        HTTPDOMAIN = `https://${req.headers.host}${req.url}`
        req.secure ? next() : res.redirect(HTTPDOMAIN)
    })
} else {
    require('dotenv').config() // set .ENV
    HTTPDOMAIN = 'http://localhost'
}

// init MONGO via MONGOOSE
// should be called after require('dotenv').config()
const mongoose = require('mongoose')
mongoose.connect(process.env.MONGO_EMPLOYERS_URI)


// set the view engine to ejs
app.set('view engine', 'ejs')
app.use(express.static(__dirname+'/'))
// uses URLENCODED for body parsing
app.use(express.urlencoded({ extended: true }))


app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'))
})

app.get('/soft', (req, res) => {
    res.sendFile(path.join(__dirname + '/about-soft.html'))
})

app.get('/about-us', (req, res) => {
    res.sendFile(path.join(__dirname + '/about-us.html'))
})

const FAQ = require('./FAQ')
app.get('/FAQ', (req, res) => {
    res.render(path.join(__dirname + '/FAQ.ejs'), {FAQ})
})


// USERS (Applicants and Students) ROUTES
app.use('/user', require('./routers/userRouter'))
// EMPLOYERS ROUTES
app.use('/employer', require('./routers/employerRouter'))


const PORT = process.env.PORT || 5000
const server = app.listen(PORT, console.log(`${HTTPDOMAIN}:${PORT}`))