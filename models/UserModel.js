const mongoose = require('mongoose')

// @UserSchema for mongoose
const userSchema = new mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },

    email: { type: String, lowercase: true, required: true },
    password: { type: String, required: true },
    
    phone: { type: String, required: true },
    cdlschool: { type: String, required: true },

    lastSESS: { type: Date, default: new Date },

    userBlock: { type: Boolean, default: false },
    adminBlock: { type: Boolean, default: false },

    uploadedFlag: { type: Boolean, default: false },
    uploadedAt: Date,
    uploadedData: {
        schoolFirstname: { type: String, default: "not uploaded" },
        schoolLastname: { type: String, default: "not uploaded" },
        schoolPhone: { type: String, default: "not uploaded" },
        schoolClass: { type: String, default: "not uploaded" },
        schoolTransmission: { type: String, default: "not uploaded" },

        schoolTuitionStartDate: { type: String, default: "not uploaded" },
        schoolTuitionStarted: Date,         //  used only for User.find, date grater/less/equal than

        schoolTuitionEndDate: { type: String, default: "not uploaded" },
        schoolTuitionEnded: Date,       //  used only for User.find, date grater/less/equal than
        
        schoolStatus: { type: String, default: "not uploaded" },
        schoolPassFail: { type: String, default: "not uploaded" },
        schoolIfDeclined: { type: String, default: "not uploaded" },
        schoolInCab: { type: String, default: "not uploaded" },
        schoolOutCab: { type: String, default: "not uploaded" },
        schoolStraightLineBacking: { type: String, default: "not uploaded" },
        schoolOffsetAndAlleyBacking: { type: String, default: "not uploaded" },
        schoolCityDriving: { type: String, default: "not uploaded" },
    },

    extraFlag: { type: Boolean, default: false },
    extraAddedAt: Date,
    extraData: {
        gender: { type: String, default: "not uploaded" },
        endorsements: { type: String, default: "not uploaded" },
        worktype: { type: String, default: "not uploaded" },
        teamwork: { type: String, default: "not uploaded" },
        distancework: { type: String, default: "not uploaded" },
        truckbrand: { type: String, default: "not uploaded" },
        yearslist: { type: String, default: "not uploaded" },
        trailertype: { type: String, default: "not uploaded" },
        paytype: { type: String, default: "not uploaded" },
        notes: String,
    },

    loadedQty: { type: Number, required: true, default: 0 },
    score: { type: Number, required: true, default: 0 },
}, {
    timestamps: true,
    collection: 'users',
})


// @EmployerSchema for mongoose
const employerSchema = new mongoose.Schema({
    // representative info
    repFirstName: { type: String, required: true },
    repLastName: { type: String, required: true },
    repPhone: { type: String, required: true },
    // company info
    companyName: { type: String, required: true },
    companyAddress: { type: String, required: true },
    companyUBI: { type: String, required: true },
    // credentials
    email: { type: String, lowercase: true, required: true },
    password: { type: String, required: true },
    // finance
    payments: [{
        whenPaid: { type: Date, default: new Date },
        amountPaid: { type: Number, default: 0.00 },
    }],
    // employer contacts
    contacts: [
        { type: mongoose.SchemaTypes.ObjectId, ref: 'userSchema' }
    ],

    lastSESS: { type: Date, default: new Date },
}, {
    timestamps: true,
    collection: 'employers',
})


// updates Student's score
function updateStudentScore(user) {
    if (!user) { return -1 }
    let score = 0
    // inc score if school data uploaded
    if (user.uploadedFlag) { 
        score += 1
        if ((new Date() - user.uploadedAt) < 100) { score += 1 }
        if (user.uploadedData.schoolTuitionEnded) {
            if (Date.parse(user.uploadedData.schoolTuitionEnded)) {
                if ((new Date() - user.uploadedData.schoolTuitionEnded) < 100) { score += 1 }
            }
        }
        // scorings
        if (user.uploadedData.schoolInCab.toUpperCase() === "PASSED") { score += 1 }
        if (user.uploadedData.schoolOutCab.toUpperCase() === "PASSED") { score += 1 }
        if (user.uploadedData.schoolStraightLineBacking.toUpperCase() === "PASSED") { score += 1 }
        if (user.uploadedData.schoolOffsetAndAlleyBacking.toUpperCase() === "PASSED") { score += 1 }
        if (user.uploadedData.schoolCityDriving.toUpperCase() === "PASSED") { score += 1 }
        // if CDL received
        if (user.uploadedData.schoolPassFail.toUpperCase() === "PASSED") { score += 10 }
    }
    // inc score if extra data added
    if (user.extraFlag) { 
        score += 3
        if (user.extraData.endorsements != "not uploaded" && user.extraData.endorsements.toUpperCase() != "NO") {
            score += 1
        }
    }
    return score
}


module.exports = { 
    User: mongoose.model('userSchema', userSchema),
    Employer: mongoose.model('employerSchema', employerSchema),
    updateStudentScore
}