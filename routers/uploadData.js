// node-fetch from v3 is an ESM-only module - you are not able to import it with require().
// If you cannot switch to ESM, please use v2
// npm install node-fetch@2
const fetch = require('node-fetch')

// config data about CDL Schools
const schoolTitles = {
    NSTS: "New Sound Trucking School",
    TTA: "Toro Trucking Academy",
    // to add one to register page, just put new school here
}
// this array will be exported
const CDLSchools = Object.values(schoolTitles).map(schoolTitle => schoolTitle)


// function to get data from NSTS with student's email
async function getDataNSTS(email) {
    const responseJSON = await fetch(process.env.EMPLOYER_SERVER_ID + `?email=${email}`)
    if (!responseJSON) { return false }

    const response = await responseJSON.json()
    if (!response) { return false }

    if (!response.length) { return false }

    return {
        schoolFirstname: response[0][0].split(' ')[0] || "no First name",
        schoolLastname: response[0][0].split(' ')[1] || "no Last name",

        schoolPhone: response[0][4] || "no Phone",
        schoolClass: response[0][6] || "no Class",
        schoolTransmission: response[0][7] || "no Transmission",

        schoolTuitionStartDate: response[0][10] || "no Tuition Start Date",
        schoolTuitionEndDate: response[0][11] || "no Tuition End Date",
        
        schoolStatus: response[0][12] || "no Status data",
        schoolPassFail: response[0][13] || "no Pass/Fail data",
        schoolIfDeclined: response[0][14] || "no Declined data",

        schoolInCab: response[0][15] || "no In-Cab test data",
        schoolOutCab: response[0][16] || "no Out-Cab test data",
        schoolStraightLineBacking: response[0][17] || "no Straight Line Backing data",
        schoolOffsetAndAlleyBacking: response[0][18] || "no Offset or Alley Dock data",
        schoolCityDriving: response[0][19] || "no City Driving test data",
    }
}


// function to get data from TTA with student's email
async function getDataTTA(email) {
    const { MongoClient } = require('mongodb')
    const client = new MongoClient(process.env.MONGO_URI_TTA_USERS)

    await client.connect()

    try {

        const agreement = await client.db('USERS')
        .collection('agreements')
        .findOne({ email }, {
            projection: { _id: 0, class: 1, transmission: 1 }
        })

        const dataCollection = await client.db('USERS')
        .collection('data-collection-forms')
        .findOne({ email }, {
            projection: { _id: 0, phone: 1 }
        })

        const student = await client.db('USERS')
        .collection('Student List')
        .findOne({ email }, {
            projection: { _id: 0, fullName: 1, created: 1, enrollmentStatus: 1, enrollmentStatusUpdate: 1, graduate: 1 }
        })

        const scorings = await client.db('USERS')
        .collection('Student Scorings')
        .findOne({ email }, {
            projection: { _id: 0, scoringsInCab: 1, scoringsOutCab: 1, scoringsBacking: 1, scoringsCity: 1 }
        })

        function getTuitionEndDate(updateDate, graduate) {
            if(!updateDate || !graduate) { return "no Tuition End Date" }
            if(graduate === "passed") { 
                return updateDate
            } else { 
                return "Not graduated yet"
            }
        }
        function getScoringData(scoringItems, notFountMsg) {
            if (!scoringItems) { return "no scoring data" }
            const scoringItem = scoringItems[scoringItems.length - 1]
            if (!scoringItem) { return notFountMsg }
            return scoringItem.result ? 'passed' : 'failed'
        }
        function getGraduateData(graduate, msg) {
            if (!graduate) { return msg }
            return graduate === 'passed' ? "Graduated"
            : graduate === 'failed' ? "Withdrew"
            : graduate === 'declined' ? "Declined"
            : graduate === 'military' ? "Military leave"
            : msg
        }

        return {
            schoolFirstname: student ? student.fullName.split(' ')[0] || "no First name" : "no student data",
            schoolLastname: student ? student.fullName.split(' ')[1] || "no Last name" : "no student data",

            schoolPhone: dataCollection ? dataCollection.phone || "no Phone" : "no Data Collection data",
            
            schoolClass: agreement ? agreement.class || "no Class" : "no Agreement data",
            schoolTransmission: agreement ? agreement.transmission || "no Transmission" : "no Agreement data",

            schoolTuitionStartDate: student ? student.created || "no Tuition Start Date" : "no student data",
            schoolTuitionEndDate: student ? getTuitionEndDate(student.enrollmentStatusUpdate, student.graduate) : "no student data",
            
            schoolStatus: student ? student.enrollmentStatus || "no Status data" : "no student data",
            schoolPassFail: student ? getGraduateData(student.graduate, "no Pass/Fail data") : "no student data",
            schoolIfDeclined: student ? getGraduateData(student.graduate, "no Declined data") : "no student data",

            schoolInCab: scorings ? getScoringData(scorings.scoringsInCab, "no In-Cab test data") : "no scoring data",
            schoolOutCab: scorings ? getScoringData(scorings.scoringsOutCab, "no Out-Cab test data") : "no scoring data",
            schoolStraightLineBacking: scorings ? getScoringData(scorings.scoringsBacking, "no Straight Line Backing data") : "no scoring data",
            schoolOffsetAndAlleyBacking: scorings ? getScoringData(scorings.scoringsBacking, "no Offset or Alley Dock data") : "no scoring data",
            schoolCityDriving: scorings ? getScoringData(scorings.scoringsCity, "no City Driving test data") : "no scoring data",
        }

    } catch(e) {
        console.error(e)
    }
    
    await client.close()

    return false
}


async function getSchoolData(email, school) {
    function getDateifAv(date) { return Date.parse(date) ? new Date(date) : new Date('2000-01-01') }

    let result = false

    if(school === schoolTitles.NSTS) {
        result = await getDataNSTS(email)
    }
    if(school === schoolTitles.TTA) {
        result = await getDataTTA(email)
    }
    if (result) {
        result.schoolTuitionStarted = getDateifAv(result.schoolTuitionStartDate)
        result.schoolTuitionEnded = getDateifAv(result.schoolTuitionEndDate)
    }

    return result    
}



module.exports = {
    CDLSchools,
    getSchoolData
}