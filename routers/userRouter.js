const express = require('express')
const session = require('express-session')

const { body, validationResult } = require('express-validator')

const path = require('path')
const bcrypt = require('bcrypt')

// module to send emails
const postman = require('../tools/postman/postman')

// import uploadData tools
const uploadDataTools = require('./uploadData')

// @SESSION setup
const SESS_DURATION = 1000 * 60 * 60 * 3    //  3 hours for Students

// extracting from process.env
const {
    NODE_ENV = 'development',

    SESS_NAME = 'sid',
    SESS_SECRET = 'BoltCDLUsers2022!',
    SESS_LIFETIME = SESS_DURATION
} = process.env

const IN_PROD = NODE_ENV === 'production'


const userRouter = express.Router()

// adding session configuration
userRouter.use(session({
    name: SESS_NAME,
    resave: false,
    saveUninitialized: false,
    secret: SESS_SECRET,
    cookie: {
        maxAge: SESS_LIFETIME,
        sameSite: true,
        secure: IN_PROD
    }
}))


// MODELS for mongoose
const { User, updateStudentScore } = require('../models/UserModel')


// middleware
function redirectToLogin (req, res, next) {
    // if user is NOT logged in, then redirect user to Login page
    if (!req.session._id) {
        res.redirect('/user/login')
    } else { next() }
}
function redirectToHome (req, res, next) {
    // if user is logged in, then redirect user to Home page
    if (req.session._id) {
        res.redirect('/user/home')
    } else { next() }
}


// without this BODY is empty when just fetching from client-side
userRouter.use(express.json({
    type: ['application/json', 'text/plain']
}))


// *ROUTES FOR USERS (applicants and Students)
userRouter.get('/', (req, res) => {
    res.redirect('/user/login')
})

userRouter.get('/login', redirectToHome, (req, res) => {
    const { issue, info } = req.query
    res.render(path.join(__dirname + '/views/userLogin.ejs'), { SESS_DURATION, issue, info })
})

userRouter.post('/login', redirectToHome, async (req, res) => {
    const { email, password } = req.body
    if (email && password) {
        const user = await User.findOneAndUpdate({ email }, { lastSESS: new Date() }) // updating last session time
        if (!user) {    //  no user with such an email
            return res.redirect('/user/login?issue=wrongUserOrPassword')  // can not find a user
        }
        try {
            if (await bcrypt.compare(password, user.password)) {
                req.session.email = user.email
                req.session._id = user._id
                return res.redirect('/user/home')
            }
        } catch(e) { res.status(500).end() }
    } 
    res.redirect('/user/login?issue=wrongUserOrPassword')  // wrong password
})


userRouter.get('/register', redirectToHome, (req, res) => {
    res.render(path.join(__dirname+'/views/userRegister.ejs'), { CDLSchools: uploadDataTools.CDLSchools})
})

userRouter.post('/register', redirectToHome,
    body("email")
    .isLength({ min: 1 }).trim().withMessage("Email must be specified")
    .isEmail().withMessage("Email must be a valid email address")
    .custom((value) => {
        return User.findOne({email : value}).then((user) => {
            if (user) {
                return Promise.reject("Email already in use");
            }
        })
    }),
    body('password')
    .isLength({ min: 5 })
    .withMessage("Minimum length is 5 symbols"),     // password must be at least 5 chars long
    async (req, res) => {
    
    // Finds the validation errors in this request
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.render(path.join(__dirname+'/views/userRegister.ejs'), { errors: errors.array() })
    }

    // Request is ok, can create a user
    const { firstname, lastname, email, password, phone, cdlschool, uploadedData } = req.body
    // check if necessary data passed
    if (!firstname || !lastname || !email || !password || !phone || !cdlschool) {
        return res.status(400).send("Necessary data not passed")
    }
    // creating a new user object
    const aNewUser = {
        firstname, lastname, email,
        password: await bcrypt.hash(password, 10),   // hashing password, salt 10
        phone, cdlschool
    }
    if (uploadedData) {
        aNewUser.uploadedFlag = true
        aNewUser.uploadedAt = new Date()
        aNewUser.uploadedData = JSON.parse(uploadedData) || {}
        aNewUser.score = updateStudentScore(aNewUser)       //  calculating score
    }
    // SAVING to a USERs collection
    try {
        new User(aNewUser).save().then((newUser) => {
                // saving is OK
                req.session.email = newUser.email
                req.session._id = newUser._id
                return res.status(200).redirect('/user/home')
            })
        }
    catch(e) {
        return res.status(500).send("Server error, try later please...", e.message)
    }
})


// Password UPDATE
userRouter.post('/password-update', redirectToLogin, async(req, res) => {
    // user is authorized, otherwise they will be redirected to LOGIN with middleware
    const { currentPassword, newPassword } = req.body
    const email = req.session.email
    const id = req.session._id

    if(!email || !id) { return  res.status(400).redirect('/user/login?issue=sessionTimeout')}

    if(!currentPassword) { return  res.status(400).redirect('/user/home?issue=blankCurrentPassword')}
    if(!newPassword) { return  res.status(400).redirect('/user/home?issue=blankNewPassword')}
    if(newPassword.length < 5) { return  res.status(400).redirect('/user/home?issue=newPasswLen')}
    if(currentPassword === newPassword) { return  res.status(400).redirect('/user/home?issue=equalCurrentNew')}

    // checking if current password passed id valid
    const user = await User.findById(id)
    if (!user) { return res.status(400).redirect('/user/login?issue=sessionTimeout')}

    try {
        if (await bcrypt.compare(currentPassword, user.password)) {
            const user = await User.findByIdAndUpdate(id, { password: await bcrypt.hash(newPassword, 10) })

            postman.sendNewPasswordLetter(user.firstname, email, newPassword, 'change')

            return res.redirect('/user/home?info=passwChanged')   //  successfuly updated
        }
        return res.status(400).redirect('/user/home?issue=wrongCurrentPassword')
    } catch(e) { res.status(500).send("Server error, try later please...") }
 
})


// Password RESET
userRouter.post('/password-reset', redirectToHome,
    body("email")
    .isLength({ min: 1 }).trim().withMessage("Email must be specified")
    .isEmail().withMessage("Email must be a valid email address")
    .custom((email) => {
        return User.findOne({ email }).then((user) => {
            if (!user) {
                return Promise.reject(`There is no user with ${email} in our database`)
            }
        })
    }), async(req, res) => {
    
    // user has to provide us with personal login-email
    const errors = validationResult(req);       // is email valid and is it a login? via 'express-validator'
    if (!errors.isEmpty()) {
        return res.render(path.join(__dirname+'/userLogin.ejs'), { errors: errors.array() })
    }

    // Request is ok, can change a user
    const { email } = req.body

    // Reseting Password
    const newPassword = postman.generateNewPassword()
    try {
        const user = await User.findOneAndUpdate({ email }, { password: await bcrypt.hash(newPassword, 10) })
        postman.sendNewPasswordLetter(user.firstname, email, newPassword, 'reset')
        res.status(200).redirect('/user/login?info=passwEmailed')
    } catch(e) {
        console.log(e)
        res.status(500).redirect('/user/login?issue=savingIssue')
    }
})



userRouter.get('/home', redirectToLogin, async(req, res) => {
    try {
        const { issue, info } = req.query
        const user = await User.findById(req.session._id).select("-password -__v")
        res.render(path.join(__dirname + '/views/userHome.ejs'), { issue, info, user })
    } catch(e) {
        res.send(`An issue occurred, when uploading profile data. Details: ${e}`)
    }
})


// upload data from specific school database
userRouter.post('/upload-school-data/:id', redirectToLogin, async(req, res) => {
    const school = req.params.id
    if (!school) { return res.status(400).redirect('/user/home?issue=schoolNotSpecified') }
    try {
        const uploadedData = await uploadDataTools.getSchoolData(req.session.email, school)
        if (!uploadedData) { return res.status(400).redirect('/user/home?issue=schoolDataNotFound') }
        // update uploaded data
        const user = await User.findByIdAndUpdate(req.session._id, {
            uploadedFlag: true,
            uploadedAt: new Date(),
            uploadedData
        })
        // if new scoring, after updating, is not equal to current scoring, then update
        const newScore = updateStudentScore(user)
        if (user.score != newScore) {
            user.score = newScore
            await user.save()
        }
        res.redirect('/user/home?info=schoolUploadedOk')
    } catch(e) {
        res.send(`An issue occurred, when uploading profile data. Details: ${e}`)
    }
})


// without this BODY is empty when just fetching from client-side
userRouter.use(express.json({
    type: ['application/json', 'text/plain']
 }))

// upload data from specific school database
userRouter.put('/upload-school-data', async(req, res) => {
    const { school, email } = req.body
    if (!school) { return res.status(400).json({ issue: "You didn't specify your CDL School" }) }
    if (!email) { return res.status(400).json({ issue: 'Email is not defined' }) }
    try {
        const uploadedData = await uploadDataTools.getSchoolData(email, school)
        if (!uploadedData) { return res.status(400).json({ issue: `${school} doesn't contain student with email ${email}` }) }
        if (uploadedData.schoolFirstname === 'no student data') { return res.status(400).json({ issue: `${school} doesn't contain student with email ${email}` }) }
        return res.status(200).json({ uploadedData })
    } catch(e) {
        return res.status(400).json({ issue: `An issue occurred, when uploading profile data. Details: ${e}` })
    }
})


// upload data from specific school database
userRouter.post('/upload-extra-data', redirectToLogin, async(req, res) => {
    const { gender, endorsements, worktype, teamwork, distancework, truckbrand, yearslist, trailertype, paytype, notes } = req.body
    const extraData = { gender, endorsements, worktype, teamwork, distancework, truckbrand, yearslist, trailertype, paytype, notes }
    try {
        const user = await User.findByIdAndUpdate(req.session._id, {
            extraFlag: true,
            extraAddedAt: new Date(),
            extraData
        })
        // if new scoring, after updating, is not equal to current scoring, then update
        const newScore = updateStudentScore(user)
        if (user.score != newScore) {
            user.score = newScore
            await user.save()
        }
        res.redirect('/user/home?info=extraUploadedOk')
    } catch(e) {
        res.send(`An issue occurred, when saving extra data. Details: ${e}`)
    }
})


// upload blocked status
userRouter.post('/update-blocked', redirectToLogin, async(req, res) => {
    try {
        const user = await User.findById(req.session._id).select("userBlock")
        if (user) {
            user.userBlock = !user.userBlock
            await user.save()
            return res.redirect('/user/home?info=blockStatusUpdatedOk')
        }
        return res.redirect('/user/home?info=blockStatusUpdatedIssue')
    } catch(e) {
        res.send(`An issue occurred, when updating block status. Details: ${e}`)
    }
})


// @ LOGOUT routes
// using this one to log user out from client side
userRouter.post('/logout', redirectToLogin, (req, res) => {
    // deleting session 
    req.session.destroy(err => {
        if (err) {
            return res.redirect('/user/home')
        }
    })
    res.clearCookie(SESS_NAME)
    res.redirect('/user/login')
})


module.exports = userRouter