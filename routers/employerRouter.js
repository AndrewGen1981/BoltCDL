const express = require('express')
const session = require('express-session')

const { body, validationResult } = require('express-validator')

const path = require('path')
const bcrypt = require('bcrypt')

const postman = require('../tools/postman/postman')

// @SESSION setup
const SESS_DURATION = 1000 * 60 * 60 * 3    //  3 hours for Students

// extracting from process.env
const {
    NODE_ENV = 'development',

    SESS_NAME = 'sid',
    SESS_SECRET = 'BoltCDLEmployers2022!',
    SESS_LIFETIME = SESS_DURATION
} = process.env

const IN_PROD = NODE_ENV === 'production'


const employerRouter = express.Router()

// adding session configuration
employerRouter.use(session({
    name: SESS_NAME,
    resave: false,
    saveUninitialized: false,
    secret: SESS_SECRET,
    cookie: {
        maxAge: SESS_LIFETIME,
        sameSite: true,
        secure: IN_PROD
    }
}))


// MODELS for mongoose
const { Employer, User } = require('../models/UserModel')

// Filters
const { filters } = require('./filtersData')


// middleware
function redirectToLogin (req, res, next) {
    // if employer is NOT logged in, then redirect employer to Login page
    if (!req.session._id) {
        res.redirect('/employer/login')
    } else { next() }
}
function redirectToHome (req, res, next) {
    // if employer is logged in, then redirect employer to Home page
    if (req.session._id) {
        res.redirect('/employer/home')
    } else { next() }
}


// without this BODY is empty when just fetching from client-side
employerRouter.use(express.json({
    type: ['application/json', 'text/plain']
}))


// *ROUTES FOR Employers (applicants and Students)
employerRouter.get('/', (req, res) => {
    res.redirect('/employer/login')
})

employerRouter.get('/login', redirectToHome, (req, res) => {
    const { issue, info } = req.query
    res.render(path.join(__dirname + '/views/employerLogin.ejs'), { SESS_DURATION, issue, info })
})

employerRouter.post('/login', redirectToHome, async (req, res) => {
    const { email, password } = req.body
    if (email && password) {
        const employer = await Employer.findOneAndUpdate({ email }, { lastSESS: new Date() }) // updating last session time
        if (!employer) {    //  no employer with such an email
            return res.redirect('/employer/login?issue=wrongUserOrPassword')  // can not find a employer
        }
        try {
            if (await bcrypt.compare(password, employer.password)) {
                req.session.email = employer.email
                req.session._id = employer._id
                // check employers debt after login
                const currentEmployer = await Employer.findById(employer._id).select("-password -__v")
                if (!currentEmployer) { return res.status(400).redirect('/employer/login?issue=sessionTimeout') }
                req.session.employer = currentEmployer
                return res.redirect('/employer/home')
            }
        } catch(e) { res.status(500).end() }
    } 
    res.redirect('/employer/login?issue=wrongUserOrPassword')  // wrong password
})


employerRouter.get('/register', redirectToHome, (req, res) => {
    res.render(path.join(__dirname+'/views/employerRegister.ejs'))
})

employerRouter.post('/register', redirectToHome,
    body("email")
    .isLength({ min: 1 }).trim().withMessage("Email must be specified")
    .isEmail().withMessage("Email must be a valid email address")
    .custom((value) => {
        return Employer.findOne({email : value}).then((employer) => {
            if (employer) {
                return Promise.reject("Email already in use");
            }
        })
    }),
    body('password')
    .isLength({ min: 5 })
    .withMessage("Minimum length is 5 symbols"),     // password must be at least 5 chars long
    async (req, res) => {
    
    // Finds the validation errors in this request
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.render(path.join(__dirname+'/views/employerRegister.ejs'), { errors: errors.array() })
    }

    // Request is ok, can create a employer
    const { repFirstName, repLastName, repPhone, companyName, companyAddress, companyUBI, email, password } = req.body
    // check if necessary data passed
    if (!repFirstName || !repLastName || !repPhone || !companyName || !companyAddress || !companyUBI || !email || !password) {
        return res.status(400).send("Necessary data not passed")
    }
    // SAVING to a Employers collection
    try {
        new Employer({
                repFirstName, repLastName, repPhone, companyName, companyAddress, companyUBI,
                email, password: await bcrypt.hash(password, 10)        // hashing password, salt 10
            }).save().then((newEmployer) => {
                // saving is OK
                req.session.email = newEmployer.email
                req.session._id = newEmployer._id
                return res.status(200).redirect('/employer/home')
            })
        }
    catch(e) {
        return res.status(500).send("Server error, try later please...", e.message)
    }
})


// Password UPDATE
employerRouter.post('/password-update', redirectToLogin, async(req, res) => {
    // employer is authorized, otherwise they will be redirected to LOGIN with middleware
    const { currentPassword, newPassword } = req.body
    const email = req.session.email
    const id = req.session._id

    if(!email || !id) { return res.status(400).redirect('/employer/login?issue=sessionTimeout')}

    if(!currentPassword) { return res.status(400).redirect('/employer/home?issue=blankCurrentPassword')}
    if(!newPassword) { return res.status(400).redirect('/employer/home?issue=blankNewPassword')}
    if(newPassword.length < 5) { return res.status(400).redirect('/employer/home?issue=newPasswLen')}
    if(currentPassword === newPassword) { return res.status(400).redirect('/employer/home?issue=equalCurrentNew')}

    // checking if current password passed id valid
    const employer = await Employer.findById(id)
    if (!employer) { return res.status(400).redirect('/employer/login?issue=sessionTimeout')}

    try {
        if (await bcrypt.compare(currentPassword, employer.password)) {
            const employer = await Employer.findByIdAndUpdate(id, { password: await bcrypt.hash(newPassword, 10) })

            postman.sendNewPasswordLetter(employer.companyName, email, newPassword, 'change')

            return res.redirect('/employer/home?info=passwChanged')   //  successfuly updated
        }
        return res.status(400).redirect('/employer/home?issue=wrongCurrentPassword')
    } catch(e) { res.status(500).send("Server error, try later please...") }
 
})


// Password RESET
employerRouter.post('/password-reset', redirectToHome,
    body("email")
    .isLength({ min: 1 }).trim().withMessage("Email must be specified")
    .isEmail().withMessage("Email must be a valid email address")
    .custom((email) => {
        return Employer.findOne({ email }).then((employer) => {
            if (!employer) {
                return Promise.reject(`There is no employer with ${email} in our database`)
            }
        })
    }), async(req, res) => {
    
    // employer has to provide us with personal login-email
    const errors = validationResult(req);       // is email valid and is it a login? via 'express-validator'
    if (!errors.isEmpty()) {
        return res.render(path.join(__dirname+'/employerLogin.ejs'), { errors: errors.array() })
    }

    // Request is ok, can change a employer
    const { email } = req.body

    // Reseting Password
    const newPassword = postman.generateNewPassword()
    try {
        const employer = await Employer.findOneAndUpdate({ email }, { password: await bcrypt.hash(newPassword, 10) })
        postman.sendNewPasswordLetter(employer.companyName, email, newPassword, 'reset')
        res.status(200).redirect('/employer/login?info=passwEmailed')
    } catch(e) {
        console.log(e)
        res.status(500).redirect('/employer/login?issue=savingIssue')
    }
})



employerRouter.get('/home', redirectToLogin, async(req, res) => {
    // check query
    const { issue, info } = req.query
    // if redirected after "login", then employer already red from database
    const emp = req.session.employer
    req.session.employer = undefined
    // loged in employer ID
    const id = req.session._id
    // avoid double reading after "login"
    const employer = emp || await Employer.findById(id).select("-password -__v")

    res.render(path.join(__dirname + '/views/employerHome.ejs'), { issue, info, employer, filters })
})


// Messenger
// route for showing all employer saved contacts
employerRouter.get('/show-saved-contacts', redirectToLogin, async(req, res) => {
    try {
        const id = req.session._id
        const employer = await Employer
        .findById(id)
        .select("-_id contacts")
        .populate({
            path: "contacts",
            select: "-_id -__v -password"
        })
        .sort({score: -1})

        res.render(path.join(__dirname + "/views/employerSavedContacts.ejs"), { employer })
    } catch(e) {
        res.status(500).send(`Issue when saving student to contacts: ${e}`)
    }
})


// route for showing filtered contacts
employerRouter.post('/show-filtered-contacts', redirectToLogin, async(req, res) => {
    const userFilters = { userBlock: false, adminBlock: false, uploadedFlag: true }
    if (filters) {
        filters.forEach(filter => {
            let tmpFilter = req.body[filter.id]
            if(tmpFilter && tmpFilter != "All") {
                userFilters[filter.field] = filter.type != "date" ? tmpFilter : { $gte: tmpFilter }
            }
        })
    }
    try {
        const id = req.session._id
        const employer = await Employer.findById(id).select("contacts")

        const users = await User.find(userFilters)
        .select("firstname lastname cdlschool extraData uploadedData score loadedQty")
        .sort({score: -1})

        res.render(path.join(__dirname + "/views/employerFilteredContacts.ejs"), { users, filters, employer, profilePrice })
    } catch(e) {
        res.status(500).send(`Issue when saving student to contacts: ${e}`)
    }
})


// setup STRIPE payments
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY)
const profilePrice = 1000    //  in cents
// const profilePrice = 50    //  test, minimum requested for stripe in cents



// add a student to employers contacts list
employerRouter.post('/checkout-contacts', redirectToLogin, async(req, res) => {
    // ids array passed from 'employerFilteredContacts.ejs'
    const { cartStudentId } = req.body
    if(cartStudentId) {
        try {
            const empId = req.session._id
            const employer = await Employer.findById(empId).select("contacts")
            // adding contacts if they are not already in there
            const newContacts = cartStudentId.filter(id => {
                if(id && !employer.contacts.includes(id)) {
                    return id
                }
            })

            if(newContacts.length) {
                // make a payment
                const session = await stripe.checkout.sessions.create({
                    payment_method_types: ['card'],
                    line_items: [
                      {
                        name: `${newContacts.length} student's profile(s)`,
                        amount: profilePrice,
                        currency: "usd",
                        quantity: newContacts.length,
                      },
                    ],
                    mode: 'payment',
                    success_url: req.headers.origin + `/employer/success?emp=${empId}&ids=${newContacts.join(',')}`,
                    cancel_url: req.headers.origin + `/employer/cancel?emp=${empId}`,
                })
                return res.redirect(303, session.url)
            } else {
                res.status(400).send("You cannot save 0 contacts")
            }
        } catch(e) {
            res.status(500).send(`Issue when saving student to contacts: ${e}`)
        }
    } else {
        res.status(400).send("Not enough data passed")
    }
})


employerRouter.get('/success', async(req, res) => {
    // cannot use redirectToLogin here, so have to check if logged in
    const id = req.query.emp
    if (!id) { return res.redirect("/employer/login") }
    // check what was passed as a new contacts
    const newIds = req.query.ids
    if (!newIds) { return res.redirect("/employer/show-saved-contacts") }
    // add new contacts to employer contacts
    try {
        // find employer record
        const employer = await Employer.findById(id).select("-password -__v")
        // working with contacts
        const newContacts = newIds.split(',')
        // updating number of record loads
        let okFlag = true
        for (let i=0; i < newContacts.length; i++) {
            let user = await User.findByIdAndUpdate(newContacts[i], { $inc: {loadedQty: 1} })
            okFlag = !user && okFlag ? false : okFlag
        }
        // quit if passed wrong user id
        if (!okFlag) {
            const issue = `Wrong contact passed`
            req.session._id = id    // this is crucial, because employer will be logged out after adding new profiles
            return res.render(path.join(__dirname + '/views/employerHome.ejs'), { issue, employer, filters })
        }
        // adding new passed contacts to existed
        employer.contacts = employer.contacts.concat(newContacts)
        // saving payments
        employer.payments.push({
            whenPaid: new Date(),
            amountPaid: profilePrice * newContacts.length
        })
        await employer.save()
        // redirecting to home
        const info = `${newContacts.length} new contact(s) successfully added`
        req.session._id = id    // this is crucial, because employer will be logged out after adding new profiles
        res.render(path.join(__dirname + '/views/employerHome.ejs'), { info, employer, filters })
    } catch(e) {
        res.status(500).send(`Issue when saving student to contacts: ${e}`)
    }
})


employerRouter.get('/cancel', async(req, res) => {
    // cannot use redirectToLogin here, so have to check if logged in
    const id = req.query.emp
    if (!id) { return res.redirect("/employer/login") }
    try {
        const employer = await Employer.findById(id).select("-password -__v")
        // this is crucial, because employer will be logged out after adding new profiles
        req.session._id = id
        res.render(path.join(__dirname + '/views/employerHome.ejs'), { employer, filters })
    } catch(e) {
        res.status(500).send(`Issue when saving student to contacts: ${e}`)
    }
})


// @ LOGOUT routes
// using this one to log employer out from client side
employerRouter.post('/logout', redirectToLogin, (req, res) => {
    // deleting session 
    req.session.destroy(err => {
        if (err) {
            return res.redirect('/employer/home')
        }
    })
    res.clearCookie(SESS_NAME)
    res.redirect('/employer/login')
})



module.exports = employerRouter