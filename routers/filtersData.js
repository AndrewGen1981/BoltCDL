const filters = [
    // uploadedData fields
    {
        type: "select",
        label: "CDL Class",
        id: "classFilter",
        list: ["All", "CDL Class A", "CDL Class B", "Upgrade to Class A", "Endorsements"],
        field: "uploadedData.schoolClass",
    },
    {
        type: "select",
        label: "Transmission",
        id: "transmissionFilter",
        list: ["All", "Automatic Transmission", "Manual Transmission"],
        field: "uploadedData.schoolTransmission",
    },
    {
        type: "date",
        label: "Tuition start date (greater than)",
        id: "tuitionStartDateFilter",
        field: "uploadedData.schoolTuitionStarted",
    },
    {
        type: "date",
        label: "Tuition end date (greater than)",
        id: "tuitionEndDateFilter",
        field: "uploadedData.schoolTuitionEnded",
    },
    {
        type: "select",
        label: "Status",
        id: "statusFilter",
        list: ["All", "Still enrolled in the program", "Graduated from the program", "Withdrew/terminated from the program", "Withdrew/terminated from the program (Declined)", "Military leave of absence"],
        field: "uploadedData.schoolStatus",
    },
    // extraData fields
    {
        type: "select",
        label: "Gender",
        id: "genderFilter",
        list: ["All", "Male", "Female"],
        field: "extraData.gender",
    },
    {
        type: "select",
        label: "Endorsements",
        id: "endorsementFilter",
        list: ["All", "Bus", "School Bus", "Tanker", "Hazmat"],
        field: "extraData.endorsements",
    },
    {
        type: "select",
        label: "Hours of work",
        id: "worktypeFilter",
        list: ["All", "Full time", "Part time"],
        field: "extraData.worktype",
    },
    {
        type: "select",
        label: "Readiness for team work",
        id: "teamworkFilter",
        list: ["All", "Yes", "No"],
        field: "extraData.teamwork",
    },
    {
        type: "select",
        label: "Distance",
        id: "distanceworkFilter",
        list: ["All", "Interstate", "Regional", "City"],
        field: "extraData.distancework",
    },
    {
        type: "select",
        label: "Truck brand",
        id: "truckbrandFilter",
        list: ["All", "Volvo", "FRTH", "PTBLT", "Western", "Kenworth"],
        field: "extraData.truckbrand",
    },
    {
        type: "text",
        label: "Truck year",
        id: "yearslistFilter",
        field: "extraData.yearslist",
    },
    {
        type: "select",
        label: "Trailer type",
        id: "trailertypeFilter",
        list: ["All", "Van", "Reffer", "Faltbad", "Carhauling"],
        field: "extraData.trailertype",
    },
    {
        type: "select",
        label: "Pay type/Amount",
        id: "paytypeFilter",
        list: ["All", "$/mile", "$/hour", "% per gross"],
        field: "extraData.paytype",
    },
]


module.exports = {
    filters
}